function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { memo, useCallback, useMemo, useRef } from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import Animated, { and, block, call, cond, eq, Extrapolate, neq, not, set, useCode } from 'react-native-reanimated';
import isEqual from 'lodash.isequal';
import { useReactiveValue } from '../../hooks';
import { DEFAULT_OPACITY, DEFAULT_APPEARS_ON_INDEX, DEFAULT_DISAPPEARS_ON_INDEX, DEFAULT_ENABLE_TOUCH_THROUGH, DEFAULT_ACCESSIBLE, DEFAULT_ACCESSIBILITY_ROLE, DEFAULT_ACCESSIBILITY_LABEL, DEFAULT_ACCESSIBILITY_HINT } from './constants';
import { WINDOW_HEIGHT } from '../../constants';
import { usePressBehavior } from './usePressBehavior';
import { styles } from './styles';

const {
  interpolate: interpolateV1,
  interpolateNode: interpolateV2
} = require('react-native-reanimated');

const interpolate = interpolateV2 || interpolateV1;
const AnimatedTouchableWithoutFeedback = Animated.createAnimatedComponent(TouchableWithoutFeedback);

const BottomSheetBackdropComponent = ({
  animatedIndex,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  animatedPosition,
  opacity = DEFAULT_OPACITY,
  appearsOnIndex = DEFAULT_APPEARS_ON_INDEX,
  disappearsOnIndex = DEFAULT_DISAPPEARS_ON_INDEX,
  enableTouchThrough = DEFAULT_ENABLE_TOUCH_THROUGH,
  pressBehavior,
  closeOnPress,
  style,
  accessible: _providedAccessible = DEFAULT_ACCESSIBLE,
  accessibilityRole: _providedAccessibilityRole = DEFAULT_ACCESSIBILITY_ROLE,
  accessibilityLabel: _providedAccessibilityLabel = DEFAULT_ACCESSIBILITY_LABEL,
  accessibilityHint: _providedAccessibilityHint = DEFAULT_ACCESSIBILITY_HINT,
  ...rest
}) => {
  //#region hooks
  const {
    handleOnPress,
    syntheticPressBehavior
  } = usePressBehavior({
    pressBehavior,
    closeOnPress,
    disappearsOnIndex
  }); //#endregion
  //#region variables

  const containerRef = useRef(null);
  const pointerEvents = useMemo(() => enableTouchThrough ? 'none' : 'auto', [enableTouchThrough]); //#endregion
  //#region animation variables

  const isTouchable = useReactiveValue(syntheticPressBehavior !== 'none' ? 1 : 0);
  const animatedOpacity = useMemo(() => interpolate(animatedIndex, {
    inputRange: [disappearsOnIndex, appearsOnIndex],
    outputRange: [0, opacity],
    extrapolate: Extrapolate.CLAMP
  }), [animatedIndex, opacity, appearsOnIndex, disappearsOnIndex]); //#endregion
  //#region styles

  const buttonStyle = useMemo(() => [style, {
    transform: [{
      translateY: cond(eq(animatedIndex, disappearsOnIndex), WINDOW_HEIGHT, 0)
    }]
  }], [disappearsOnIndex, style, animatedIndex]);
  const containerStyle = useMemo(() => [styles.container, style, {
    opacity: animatedOpacity
  }], [style, animatedOpacity]); //#endregion

  const setPointerEvents = useCallback(value => {
    if (containerRef.current) {
      containerRef.current.setNativeProps({
        pointerEvents: value
      });
    }
  }, []); //#region effects

  useCode(() => block([cond(and(eq(animatedIndex, disappearsOnIndex), isTouchable), [set(isTouchable, 0), call([], () => setPointerEvents('none'))], cond(and(neq(animatedIndex, disappearsOnIndex), not(isTouchable)), [set(isTouchable, 1), call([], () => setPointerEvents('auto'))]))]), [animatedIndex, disappearsOnIndex, isTouchable, setPointerEvents]); //#endregion

  return syntheticPressBehavior !== 'none' ? /*#__PURE__*/React.createElement(AnimatedTouchableWithoutFeedback, _extends({
    accessible: _providedAccessible !== null && _providedAccessible !== void 0 ? _providedAccessible : undefined,
    accessibilityRole: _providedAccessibilityRole !== null && _providedAccessibilityRole !== void 0 ? _providedAccessibilityRole : undefined,
    accessibilityLabel: _providedAccessibilityLabel !== null && _providedAccessibilityLabel !== void 0 ? _providedAccessibilityLabel : undefined,
    accessibilityHint: _providedAccessibilityHint !== null && _providedAccessibilityHint !== void 0 ? _providedAccessibilityHint : undefined,
    style: buttonStyle,
    onPress: handleOnPress
  }, rest), /*#__PURE__*/React.createElement(Animated.View, {
    ref: containerRef,
    key: "backdrop",
    style: containerStyle
  })) : /*#__PURE__*/React.createElement(Animated.View, _extends({
    ref: containerRef,
    key: "backdrop",
    accessible: _providedAccessible !== null && _providedAccessible !== void 0 ? _providedAccessible : undefined,
    accessibilityRole: _providedAccessibilityRole !== null && _providedAccessibilityRole !== void 0 ? _providedAccessibilityRole : undefined,
    accessibilityLabel: _providedAccessibilityLabel !== null && _providedAccessibilityLabel !== void 0 ? _providedAccessibilityLabel : undefined,
    accessibilityHint: _providedAccessibilityHint !== null && _providedAccessibilityHint !== void 0 ? _providedAccessibilityHint : undefined,
    pointerEvents: pointerEvents,
    style: containerStyle
  }, rest));
};

const BottomSheetBackdrop = /*#__PURE__*/memo(BottomSheetBackdropComponent, isEqual);
export default BottomSheetBackdrop;
//# sourceMappingURL=BottomSheetBackdrop.js.map