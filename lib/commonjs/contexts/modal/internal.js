"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BottomSheetModalInternalProvider = exports.BottomSheetModalInternalContext = void 0;

var _react = require("react");

// @ts-ignore
const BottomSheetModalInternalContext = /*#__PURE__*/(0, _react.createContext)();
exports.BottomSheetModalInternalContext = BottomSheetModalInternalContext;
const BottomSheetModalInternalProvider = BottomSheetModalInternalContext.Provider;
exports.BottomSheetModalInternalProvider = BottomSheetModalInternalProvider;
//# sourceMappingURL=internal.js.map