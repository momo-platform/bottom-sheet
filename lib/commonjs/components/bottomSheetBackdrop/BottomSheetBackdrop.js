"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactNative = require("react-native");

var _reactNativeReanimated = _interopRequireWildcard(require("react-native-reanimated"));

var _lodash = _interopRequireDefault(require("lodash.isequal"));

var _hooks = require("../../hooks");

var _constants = require("./constants");

var _constants2 = require("../../constants");

var _usePressBehavior = require("./usePressBehavior");

var _styles = require("./styles");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

const {
  interpolate: interpolateV1,
  interpolateNode: interpolateV2
} = require('react-native-reanimated');

const interpolate = interpolateV2 || interpolateV1;

const AnimatedTouchableWithoutFeedback = _reactNativeReanimated.default.createAnimatedComponent(_reactNative.TouchableWithoutFeedback);

const BottomSheetBackdropComponent = ({
  animatedIndex,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  animatedPosition,
  opacity = _constants.DEFAULT_OPACITY,
  appearsOnIndex = _constants.DEFAULT_APPEARS_ON_INDEX,
  disappearsOnIndex = _constants.DEFAULT_DISAPPEARS_ON_INDEX,
  enableTouchThrough = _constants.DEFAULT_ENABLE_TOUCH_THROUGH,
  pressBehavior,
  closeOnPress,
  style,
  accessible: _providedAccessible = _constants.DEFAULT_ACCESSIBLE,
  accessibilityRole: _providedAccessibilityRole = _constants.DEFAULT_ACCESSIBILITY_ROLE,
  accessibilityLabel: _providedAccessibilityLabel = _constants.DEFAULT_ACCESSIBILITY_LABEL,
  accessibilityHint: _providedAccessibilityHint = _constants.DEFAULT_ACCESSIBILITY_HINT,
  ...rest
}) => {
  //#region hooks
  const {
    handleOnPress,
    syntheticPressBehavior
  } = (0, _usePressBehavior.usePressBehavior)({
    pressBehavior,
    closeOnPress,
    disappearsOnIndex
  }); //#endregion
  //#region variables

  const containerRef = (0, _react.useRef)(null);
  const pointerEvents = (0, _react.useMemo)(() => enableTouchThrough ? 'none' : 'auto', [enableTouchThrough]); //#endregion
  //#region animation variables

  const isTouchable = (0, _hooks.useReactiveValue)(syntheticPressBehavior !== 'none' ? 1 : 0);
  const animatedOpacity = (0, _react.useMemo)(() => interpolate(animatedIndex, {
    inputRange: [disappearsOnIndex, appearsOnIndex],
    outputRange: [0, opacity],
    extrapolate: _reactNativeReanimated.Extrapolate.CLAMP
  }), [animatedIndex, opacity, appearsOnIndex, disappearsOnIndex]); //#endregion
  //#region styles

  const buttonStyle = (0, _react.useMemo)(() => [style, {
    transform: [{
      translateY: (0, _reactNativeReanimated.cond)((0, _reactNativeReanimated.eq)(animatedIndex, disappearsOnIndex), _constants2.WINDOW_HEIGHT, 0)
    }]
  }], [disappearsOnIndex, style, animatedIndex]);
  const containerStyle = (0, _react.useMemo)(() => [_styles.styles.container, style, {
    opacity: animatedOpacity
  }], [style, animatedOpacity]); //#endregion

  const setPointerEvents = (0, _react.useCallback)(value => {
    if (containerRef.current) {
      containerRef.current.setNativeProps({
        pointerEvents: value
      });
    }
  }, []); //#region effects

  (0, _reactNativeReanimated.useCode)(() => (0, _reactNativeReanimated.block)([(0, _reactNativeReanimated.cond)((0, _reactNativeReanimated.and)((0, _reactNativeReanimated.eq)(animatedIndex, disappearsOnIndex), isTouchable), [(0, _reactNativeReanimated.set)(isTouchable, 0), (0, _reactNativeReanimated.call)([], () => setPointerEvents('none'))], (0, _reactNativeReanimated.cond)((0, _reactNativeReanimated.and)((0, _reactNativeReanimated.neq)(animatedIndex, disappearsOnIndex), (0, _reactNativeReanimated.not)(isTouchable)), [(0, _reactNativeReanimated.set)(isTouchable, 1), (0, _reactNativeReanimated.call)([], () => setPointerEvents('auto'))]))]), [animatedIndex, disappearsOnIndex, isTouchable, setPointerEvents]); //#endregion

  return syntheticPressBehavior !== 'none' ? /*#__PURE__*/_react.default.createElement(AnimatedTouchableWithoutFeedback, _extends({
    accessible: _providedAccessible !== null && _providedAccessible !== void 0 ? _providedAccessible : undefined,
    accessibilityRole: _providedAccessibilityRole !== null && _providedAccessibilityRole !== void 0 ? _providedAccessibilityRole : undefined,
    accessibilityLabel: _providedAccessibilityLabel !== null && _providedAccessibilityLabel !== void 0 ? _providedAccessibilityLabel : undefined,
    accessibilityHint: _providedAccessibilityHint !== null && _providedAccessibilityHint !== void 0 ? _providedAccessibilityHint : undefined,
    style: buttonStyle,
    onPress: handleOnPress
  }, rest), /*#__PURE__*/_react.default.createElement(_reactNativeReanimated.default.View, {
    ref: containerRef,
    key: "backdrop",
    style: containerStyle
  })) : /*#__PURE__*/_react.default.createElement(_reactNativeReanimated.default.View, _extends({
    ref: containerRef,
    key: "backdrop",
    accessible: _providedAccessible !== null && _providedAccessible !== void 0 ? _providedAccessible : undefined,
    accessibilityRole: _providedAccessibilityRole !== null && _providedAccessibilityRole !== void 0 ? _providedAccessibilityRole : undefined,
    accessibilityLabel: _providedAccessibilityLabel !== null && _providedAccessibilityLabel !== void 0 ? _providedAccessibilityLabel : undefined,
    accessibilityHint: _providedAccessibilityHint !== null && _providedAccessibilityHint !== void 0 ? _providedAccessibilityHint : undefined,
    pointerEvents: pointerEvents,
    style: containerStyle
  }, rest));
};

const BottomSheetBackdrop = /*#__PURE__*/(0, _react.memo)(BottomSheetBackdropComponent, _lodash.default);
var _default = BottomSheetBackdrop;
exports.default = _default;
//# sourceMappingURL=BottomSheetBackdrop.js.map