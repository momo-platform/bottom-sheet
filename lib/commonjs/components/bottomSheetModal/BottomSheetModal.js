"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _portal = require("@gorhom/portal");

var _nonSecure = require("nanoid/non-secure");

var _lodash = _interopRequireDefault(require("lodash.isequal"));

var _bottomSheet = _interopRequireDefault(require("../bottomSheet"));

var _hooks = require("../../hooks");

var _constants = require("./constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

const BottomSheetModalComponent = /*#__PURE__*/(0, _react.forwardRef)((props, ref) => {
  const {
    // modal props
    name,
    dismissOnPanDown = _constants.DEFAULT_DISMISS_ON_PAN_DOWN,
    onDismiss: _providedOnDismiss,
    // bottom sheet props
    index: _providedIndex = 0,
    snapPoints: _providedSnapPoints,
    onChange: _providedOnChange,
    topInset = 0,
    bottomInset = 0,
    // components
    children,
    ...bottomSheetProps
  } = props; //#region state

  const [mount, setMount] = (0, _react.useState)(false); //#endregion
  //#region hooks

  const {
    containerHeight,
    mountSheet,
    unmountSheet,
    willUnmountSheet
  } = (0, _hooks.useBottomSheetModalInternal)();
  const {
    removePortal: unmountPortal
  } = (0, _portal.usePortal)(); //#endregion
  //#region refs

  const bottomSheetRef = (0, _react.useRef)(null);
  const currentIndexRef = (0, _react.useRef)(-1);
  const restoreIndexRef = (0, _react.useRef)(-1);
  const minimized = (0, _react.useRef)(false);
  const forcedDismissed = (0, _react.useRef)(false);
  const mounted = (0, _react.useRef)(true); //#endregion
  //#region variables

  const key = (0, _react.useMemo)(() => name || "bottom-sheet-modal-".concat((0, _nonSecure.nanoid)()), [name]);
  const index = (0, _react.useMemo)(() => dismissOnPanDown ? _providedIndex + 1 : _providedIndex, [_providedIndex, dismissOnPanDown]);
  const snapPoints = (0, _react.useMemo)(() => dismissOnPanDown ? [0, ..._providedSnapPoints] : _providedSnapPoints, [_providedSnapPoints, dismissOnPanDown]);
  const safeContainerHeight = (0, _react.useMemo)(() => containerHeight - topInset - bottomInset, [containerHeight, topInset, bottomInset]); //#endregion
  //#region private methods

  const resetVariables = (0, _react.useCallback)(() => {
    currentIndexRef.current = -1;
    restoreIndexRef.current = -1;
    minimized.current = false;
    mounted.current = true;
    forcedDismissed.current = false;
  }, []);
  const adjustIndex = (0, _react.useCallback)(_index => dismissOnPanDown ? _index - 1 : _index, [dismissOnPanDown]);
  const unmount = (0, _react.useCallback)(() => {
    const _mounted = mounted.current; // reset variables

    resetVariables(); // unmount sheet and portal

    unmountSheet(key);
    unmountPortal(key); // unmount the node, if sheet is still mounted

    if (_mounted) {
      setMount(false);
    } // fire `onDismiss` callback


    if (_providedOnDismiss) {
      _providedOnDismiss();
    }
  }, [key, resetVariables, unmountSheet, unmountPortal, _providedOnDismiss]); //#endregion
  //#region bottom sheet methods

  const handleSnapTo = (0, _react.useCallback)(_index => {
    var _bottomSheetRef$curre;

    if (minimized.current) {
      return;
    }

    (_bottomSheetRef$curre = bottomSheetRef.current) === null || _bottomSheetRef$curre === void 0 ? void 0 : _bottomSheetRef$curre.snapTo(adjustIndex(_index));
  }, [adjustIndex]);
  const handleExpand = (0, _react.useCallback)(() => {
    var _bottomSheetRef$curre2;

    if (minimized.current) {
      return;
    }

    (_bottomSheetRef$curre2 = bottomSheetRef.current) === null || _bottomSheetRef$curre2 === void 0 ? void 0 : _bottomSheetRef$curre2.expand();
  }, []);
  const handleCollapse = (0, _react.useCallback)(() => {
    if (minimized.current) {
      return;
    }

    if (dismissOnPanDown) {
      var _bottomSheetRef$curre3;

      (_bottomSheetRef$curre3 = bottomSheetRef.current) === null || _bottomSheetRef$curre3 === void 0 ? void 0 : _bottomSheetRef$curre3.snapTo(1);
    } else {
      var _bottomSheetRef$curre4;

      (_bottomSheetRef$curre4 = bottomSheetRef.current) === null || _bottomSheetRef$curre4 === void 0 ? void 0 : _bottomSheetRef$curre4.collapse();
    }
  }, [dismissOnPanDown]);
  const handleClose = (0, _react.useCallback)(() => {
    var _bottomSheetRef$curre5;

    if (minimized.current) {
      return;
    }

    (_bottomSheetRef$curre5 = bottomSheetRef.current) === null || _bottomSheetRef$curre5 === void 0 ? void 0 : _bottomSheetRef$curre5.close();
  }, []); //#endregion
  //#region bottom sheet modal methods

  const handlePresent = (0, _react.useCallback)(() => {
    requestAnimationFrame(() => {
      setMount(true);
      mountSheet(key, ref);
    });
  }, [key, ref, mountSheet]);
  const handleDismiss = (0, _react.useCallback)(() => {
    var _bottomSheetRef$curre6;

    /**
     * if modal is already been dismiss, we exit the method.
     */
    if (currentIndexRef.current === -1 && minimized.current === false) {
      return;
    }

    if (minimized.current) {
      unmount();
      return;
    }

    willUnmountSheet(key);
    forcedDismissed.current = true;
    (_bottomSheetRef$curre6 = bottomSheetRef.current) === null || _bottomSheetRef$curre6 === void 0 ? void 0 : _bottomSheetRef$curre6.close();
  }, [willUnmountSheet, unmount, key]);
  const handleMinimize = (0, _react.useCallback)(() => {
    var _bottomSheetRef$curre7;

    if (minimized.current) {
      return;
    }

    minimized.current = true;
    /**
     * if modal got minimized before it finish its mounting
     * animation, we set the `restoreIndexRef` to the
     * provided index.
     */

    if (currentIndexRef.current === -1) {
      restoreIndexRef.current = index;
    } else {
      restoreIndexRef.current = currentIndexRef.current;
    }

    (_bottomSheetRef$curre7 = bottomSheetRef.current) === null || _bottomSheetRef$curre7 === void 0 ? void 0 : _bottomSheetRef$curre7.close();
  }, [index]);
  const handleRestore = (0, _react.useCallback)(() => {
    var _bottomSheetRef$curre8;

    if (!minimized.current || forcedDismissed.current) {
      return;
    }

    minimized.current = false;
    (_bottomSheetRef$curre8 = bottomSheetRef.current) === null || _bottomSheetRef$curre8 === void 0 ? void 0 : _bottomSheetRef$curre8.snapTo(restoreIndexRef.current);
  }, []); //#endregion
  //#region callbacks

  const handlePortalOnUnmount = (0, _react.useCallback)(() => {
    var _bottomSheetRef$curre9;

    /**
     * if modal is already been dismiss, we exit the method.
     */
    if (currentIndexRef.current === -1 && minimized.current === false) {
      return;
    }

    mounted.current = false;
    forcedDismissed.current = true;

    if (minimized.current) {
      unmount();
      return;
    }

    willUnmountSheet(key);
    (_bottomSheetRef$curre9 = bottomSheetRef.current) === null || _bottomSheetRef$curre9 === void 0 ? void 0 : _bottomSheetRef$curre9.close();
  }, [key, unmount, willUnmountSheet]);
  const handleBottomSheetOnChange = (0, _react.useCallback)(_index => {
    const adjustedIndex = adjustIndex(_index);
    currentIndexRef.current = _index;

    if (_providedOnChange) {
      _providedOnChange(adjustedIndex);
    }

    if (minimized.current) {
      return;
    }

    if (adjustedIndex === -1) {
      unmount();
    }
  }, [adjustIndex, unmount, _providedOnChange]); //#endregion
  //#region expose public methods

  (0, _react.useImperativeHandle)(ref, () => ({
    // sheet
    snapTo: handleSnapTo,
    expand: handleExpand,
    collapse: handleCollapse,
    close: handleClose,
    // modal
    present: handlePresent,
    dismiss: handleDismiss,
    // private
    minimize: handleMinimize,
    restore: handleRestore
  })); //#endregion
  // render

  return mount ? /*#__PURE__*/_react.default.createElement(_portal.Portal, {
    key: key,
    name: key,
    handleOnUnmount: handlePortalOnUnmount
  }, /*#__PURE__*/_react.default.createElement(_bottomSheet.default, _extends({}, bottomSheetProps, {
    ref: bottomSheetRef,
    key: key,
    index: index,
    snapPoints: snapPoints,
    animateOnMount: true,
    topInset: topInset,
    bottomInset: bottomInset,
    containerHeight: safeContainerHeight,
    onChange: handleBottomSheetOnChange,
    children: children
  }))) : null;
});
const BottomSheetModal = /*#__PURE__*/(0, _react.memo)(BottomSheetModalComponent, _lodash.default);
var _default = BottomSheetModal;
exports.default = _default;
//# sourceMappingURL=BottomSheetModal.js.map