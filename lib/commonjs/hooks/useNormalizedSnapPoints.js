"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useNormalizedSnapPoints = void 0;

var _react = require("react");

var _utilities = require("../utilities");

const useNormalizedSnapPoints = (snapPoints, containerHeight = 0, handleHeight = 0) => (0, _react.useMemo)(() => {
  const normalizedSnapPoints = (0, _utilities.normalizeSnapPoints)(snapPoints, containerHeight);
  return normalizedSnapPoints.map(normalizedSnapPoint => {
    /**
     * we subset handleHeight from the `normalizedSnapPoint` to make
     * sure that sheets and its handle will be out of the screen.
     */
    if (normalizedSnapPoint === 0 && handleHeight !== 0) {
      normalizedSnapPoint = normalizedSnapPoint - handleHeight;
    }

    return Math.max(containerHeight - normalizedSnapPoint - handleHeight, 0);
  });
}, [snapPoints, containerHeight, handleHeight]);

exports.useNormalizedSnapPoints = useNormalizedSnapPoints;
//# sourceMappingURL=useNormalizedSnapPoints.js.map