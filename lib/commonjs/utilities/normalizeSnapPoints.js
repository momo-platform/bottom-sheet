"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.normalizeSnapPoints = void 0;

var _validateSnapPoint = require("./validateSnapPoint");

/**
 * Converts snap points with percentage to fixed numbers.
 */
const normalizeSnapPoints = (snapPoints, containerHeight) => snapPoints.map(snapPoint => {
  (0, _validateSnapPoint.validateSnapPoint)(snapPoint);
  return typeof snapPoint === 'number' ? snapPoint : Number(snapPoint.split('%')[0]) * containerHeight / 100;
});

exports.normalizeSnapPoints = normalizeSnapPoints;
//# sourceMappingURL=normalizeSnapPoints.js.map