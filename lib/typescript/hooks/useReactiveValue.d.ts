import Animated from 'react-native-reanimated';
export declare const useReactiveValue: (value: number) => Animated.Value<number>;
