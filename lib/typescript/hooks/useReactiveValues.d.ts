import Animated from 'react-native-reanimated';
export declare const useReactiveValues: (values: ReadonlyArray<number>) => Animated.Value<number>[];
