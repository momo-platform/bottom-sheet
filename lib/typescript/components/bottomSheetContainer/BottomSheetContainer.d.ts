import React from 'react';
import type { BottomSheetContainerProps } from './types';
declare const BottomSheetContainer: React.MemoExoticComponent<({ shouldMeasureHeight, onMeasureHeight, children, topInset, bottomInset, }: BottomSheetContainerProps) => JSX.Element>;
export default BottomSheetContainer;
