export declare const styles: {
    container: {
        position: "absolute";
        left: number;
        top: number;
        padding: number;
        backgroundColor: string;
    };
    text: {
        fontSize: number;
        lineHeight: number;
        height: number;
        padding: number;
        color: string;
    };
};
