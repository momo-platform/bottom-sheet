import React from 'react';
import type { BottomSheetViewProps } from './types';
declare const BottomSheetView: React.MemoExoticComponent<({ style, focusHook: useFocusHook, children, ...reset }: BottomSheetViewProps) => JSX.Element>;
export default BottomSheetView;
