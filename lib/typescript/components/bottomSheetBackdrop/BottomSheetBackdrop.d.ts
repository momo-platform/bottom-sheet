import React from 'react';
import type { BottomSheetDefaultBackdropProps } from './types';
declare const BottomSheetBackdrop: React.MemoExoticComponent<({ animatedIndex, animatedPosition, opacity, appearsOnIndex, disappearsOnIndex, enableTouchThrough, pressBehavior, closeOnPress, style, accessible: _providedAccessible, accessibilityRole: _providedAccessibilityRole, accessibilityLabel: _providedAccessibilityLabel, accessibilityHint: _providedAccessibilityHint, ...rest }: BottomSheetDefaultBackdropProps) => JSX.Element>;
export default BottomSheetBackdrop;
